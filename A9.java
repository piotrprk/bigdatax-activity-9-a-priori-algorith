
public class A9 {
	public static void main(String[] args) {
		String file_name = "dataset.txt";
		//
		Transactions T = new Transactions(file_name);
		T.printTransactions();
		// set threshold
		int s = 2;
		// print answer
		System.out.println("Support treshold: " + s);
		System.out.println();
		T.frequentItrems(s);
		//
		System.out.println();
		T.confidenceAll(2, 0.1);
	}
}
