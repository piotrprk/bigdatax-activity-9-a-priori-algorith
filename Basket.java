import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

public class Basket {
	// to keep shopping list
	// key = name of the product
	// value = number of product items
	HashMap<String, Integer> basket;
	
	public Basket() {
		this.basket = new HashMap<>();
	}
	
	public String toString() {
		String out = new String();
		int i = 0;
		for (String k:this.basket.keySet()) {
			out += k;
			if (i++ < this.basket.size() -1 )
				out += ", ";
		}
		return out;
	}
}

class Transactions{
	// keep all transactions in a table
	ArrayList<Basket> transactions;
	
	public Transactions() {
		this.transactions = new ArrayList<>();
	}
	public Transactions(String file_name) {
		this.transactions = new ArrayList<>();
		this.loadDataRaw(file_name);
	}
	
	// import file structure
	// item_name TAB item_name TAB item_name
	// each basket in a new line
	public void loadDataRaw(String file_name) {
		// get the data
		File f = new File(file_name);

		try (Scanner scanner = new Scanner(f)){	
			while(scanner.hasNextLine()) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// split line
					String[] line = i.split("\\t");	// get data by split by TAB
					// add file text line as basket
					this.addBasket(line);
				}	
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}					
	}
	private void addBasket(String[] items) {
		Basket basket = new Basket();
		for(String s:items) {
			int n = 0;
			if ( basket.basket.containsKey(s) ) {
				n = basket.basket.get(s);
			}
			basket.basket.put(s, ++n);
		}
		this.transactions.add(basket);
	}
	public void printTransactions() {
		int i = 0;
		for (Basket b:this.transactions) {
			System.out.print(++i + "\t");
			System.out.print(b + "\n");
		}
	}
	// compute frequent items
	// set s as support threshold
	public void frequentItrems(int s) {
		TreeMap<String, Integer> singletons = new TreeMap<>();
		// compute singletons and its support
		for (Basket b:this.transactions) {
			for (String k:b.basket.keySet()) {
				int n = 0;
				if (singletons.containsKey(k)) {
					n = singletons.get(k);
				}	
				singletons.put(k, ++n);
			}		
		}
		// remove singletons with support < s
		ArrayList<String> to_remove = new ArrayList<>();
		for (String k:singletons.keySet()) {
			if (singletons.get(k) < s)
				to_remove.add(k);				
		}
		for (String k:to_remove) {
			singletons.remove(k);
		}
		// print singletons
		System.out.println();
		for (String k:singletons.keySet()) {
			System.out.println(k + " : " + singletons.get(k));
		}
		/*
		// step 2 - pairs
		TreeMap<String, Integer> itemsets = new TreeMap<>();
		ArrayList<String> keys = new ArrayList<>( singletons.keySet() );
		// generate pairs
		for (int i=0; i<keys.size(); i++) {
			for (int j=i+1; j<keys.size(); j++) {
				// item A
				String A = keys.get(i);
				// item B
				String B = keys.get(j);
				// check in every basket for this pair
				int count = 0;
				for (Basket b:this.transactions) {
					if(b.basket.containsKey(A)) {
						if(b.basket.containsKey(B))
							count++;
					}
				}
				
				if (count >= s) {
					// pair
					String pair = A + "," + B;		
					
					itemsets.put(pair, count);
				}
			}
		}			
		*/
		// 
		
		int steps = 4;
		int step = 1;
		ArrayList< TreeMap<String, Integer> > sets = new ArrayList<>();
		//
		HashMap<String, Double> support = new HashMap<>();
		HashMap<String, Double> confidence = new HashMap<>();
		int N = this.transactions.size();
		//
		// sets[0] = singletons
		sets.add(singletons);
		ArrayList<String> single_keys = new ArrayList<>( singletons.keySet() );
		//
		while( sets.get(step-1).size() > 0) {
			
			TreeMap<String, Integer> set = new TreeMap<>();
			
			ArrayList<String> keys = new ArrayList<>( sets.get(step-1).keySet() );
						
			for (int i=0; i< keys.size(); i++) {
				for (int j=0; j<single_keys.size(); j++) {
					//
					int count = 0;
					String[] A_set = keys.get(i).split("-");
					TreeSet<String> A_tree = new TreeSet<>();
					for(String A:A_set) {
						A_tree.add(A);
					}
					
					// item B
					String B = single_keys.get(j);
					//
					TreeSet<String> B_tree = new TreeSet<>(A_tree);
					B_tree.add(B);
					//
					String set_key = "";
					int n = 0;
					for (String A:B_tree) {
						set_key += A;
						if(n++<B_tree.size()-1)
							set_key += "-";
					}

					//
					int support_A_tree = 0;
					if (!A_tree.contains(B) && !set.containsKey(set_key)) {
						// check in every basket for this set of items A_set						
						for (Basket b:this.transactions) {					
							int contains_set = 0;
							for(String A:A_set) {
								 if (b.basket.containsKey(A) )
									 contains_set++;
							}
							if(contains_set == A_set.length) {
								if(b.basket.containsKey(B))
									count++;
							}
						}	
						// what is support(X) ?
						String A_tree_key = "";
						int k = 0;
						for (String A:A_tree) {
							A_tree_key += A;
							if(k++<A_tree.size()-1)
								A_tree_key += "-";
						}				
						if (sets.get(step-1).containsKey(A_tree_key))
							support_A_tree = sets.get(step-1).get(A_tree_key);
					}
					
					if (count >= s) {
						// pair	
						set.put(set_key, count);
						// support(X->Y) = support(X and Y) / N
						support.put(set_key, (double)count / N);
						// confidence(X->Y) =  support(X and Y) / support(X)
						if (support_A_tree != 0)
							confidence.put(set_key, (double)count / support_A_tree);
					}
				}
			}									
			sets.add(set);
			// print 
			System.out.println();
			System.out.println("step: " + step);
			for (String k:set.keySet()) {
				System.out.println(k + 
						" : " + set.get(k) + 
						" support: " + support.get(k) +
						" confidence: " + confidence.get(k)
						);
			}
			//
			step++;
		}		
	}
	public double confidence(ArrayList<String> A, ArrayList<String> B) {
		int deltaA = 0;
		int deltaAB = 0;
		for (Basket basket:this.transactions) {					
			int contains_set = 0;
			for(String a:A) {
				 if (basket.basket.containsKey(a) )
					 contains_set++;
			}
			if(contains_set == A.size()) {
				deltaA++;
				contains_set = 0;
				for(String b:B) {
					 if (basket.basket.containsKey(b) )
						 contains_set++;
				}				
				if(contains_set == B.size())
					deltaAB++;
			}
		}		
		return (double) deltaAB / deltaA;
	}	
	public void confidenceAll(int s, double treshold) {
		// get all items
		TreeMap<String, Integer> items = new TreeMap<>();
		for (Basket b:this.transactions) {
			for (String k:b.basket.keySet()) {
				int n = 0;
				if (items.containsKey(k))
					n = items.get(k);				
				items.put(k, ++n);
			}			
		}
		// remove singletons with support < s
		ArrayList<String> to_remove = new ArrayList<>();
		for (String k:items.keySet()) {
			if (items.get(k) < s)
				to_remove.add(k);				
		}
		for (String k:to_remove) {
			items.remove(k);
		}

		//
		ArrayList < HashMap<String, Double> > confidence = new ArrayList<>();
		//
		ArrayList<String> keys = new ArrayList<>(items.keySet());
		//
		HashMap<String, Double> map = new HashMap<>();
		for (String k:keys) {
			map.put(k, 0d);
		}
		confidence.add(map);
		
		int counter = 0;
		int steps = 4;
		int step = 0;
		while(step < steps) {
			
			HashMap<String, Double> confidence_map = new HashMap<>();
			
			for (String keyA:confidence.get(step).keySet()) {
				
				ArrayList<String> A = new ArrayList<>();
				for (String str:keyA.split("-")){
					A.add(str);
				}
				
				for (String keyB:keys) {
					
					if ( !A.contains(keyB)) {
	
						ArrayList<String> B = new ArrayList<>();
						B.add( keyB );
						//
						double d = this.confidence( A, B );
						//
						String set_key = "";
						int k = 0;
						ArrayList<String> AB = new ArrayList<>(A);
						AB.addAll(B);
						for (String a:AB) {
							set_key += a;
							if(k++ < AB.size()-1)
								set_key += "-";
						}		
						int countSet = this.countSet(AB);
						if ( countSet >= s) {
							confidence_map.put(set_key, d);
						}
					}
				}
			}
			confidence.add(confidence_map);
			for (String k:confidence_map.keySet()) {
				if ( confidence_map.get(k) >= treshold )
					System.out.println(++counter + " confidence " + k + " : " + confidence_map.get(k));
			}
			step++;
		}
	}
	private int countSet(ArrayList<String> Set) {
		int countSet=0;
		for (Basket b:transactions) {
			int count = 0;
			for (String s:Set) {
				if (b.basket.containsKey(s))
					count++;
			}
			if (count == Set.size())
				countSet++;
		}
		return countSet;
	}
}
